package Test;

import Computer.Computer;
import Computer.Factorial;
import Computer.LongMemory;
import Computer.Program;
/**
 * ComputerTester class is a class to test the factorial program and all its toString methods.
 * @author EDA061
 *
 */
public class ComputerTester {
	public static void main(String[] args) {
		Program factorial = new Factorial();
	    System.out.println(factorial);
	    Computer computer = new Computer(new LongMemory(1024));
	    computer.load(factorial);
	    computer.run();
	}
}
