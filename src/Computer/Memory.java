package Computer;

/**
 * Memory class is an abstract class that works as the computers memory it has a
 * length and methods to manipulate the memory.
 * 
 * @author Alexander Löfqvist
 *
 */
public abstract class Memory {
	protected int length;
	protected Word[] memArray;

	/**
	 * Construct.
	 * 
	 * @param length
	 *            integer that specifies the length of the memory.
	 */
	protected Memory(int length) {
		this.length = length;
	}

	/**
	 * Method to read the memory at a certain position.
	 * 
	 * @param index
	 *            integer where the memory should be read.
	 * @return Word, returns the word at the given position.
	 * @throws java.lang.IndexOutOfBoundsException
	 */
	public Word read(int index) throws java.lang.IndexOutOfBoundsException {
		try {
			return this.memArray[index];
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Index out of bounds. Tried to read on memory position: " + index
					+ ", length of the memory is only: " + this.length);
			return null;
		}
	}

	/**
	 * Method to write a word at a certain position in the memory.
	 * 
	 * @param index
	 *            integer that is the index in the memory where the word should
	 *            be written.
	 * @param w
	 *            Word object which is the word that should be written.
	 * @return true if write was possible otherwise false.
	 */
	public boolean write(int index, Word w) {
		try {
			this.memArray[index] = w;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Index out of bounds. Tried to write on memory position: " + index
					+ ", length of the memory is only: " + this.length);
			return false;
		}
		return true;
	}
}
