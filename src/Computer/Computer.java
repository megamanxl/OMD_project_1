package Computer;

/**
 * Computer class is constructed with an memory. It have methods to allow the
 * class to load a program, and method to run the computer.
 * 
 * @author Alexander Löfqvist
 *
 */
public class Computer {
	private Memory mem;
	private Program prog;
	private Counter counter;

	/**
	 * Construct
	 * 
	 * @param m
	 *            Memory object which is the memory the computer is assigned.
	 */
	public Computer(Memory m) {
		this.mem = m;
	}

	/**
	 * Load method loads a program and creates a program counter in the form of
	 * a Counter object.
	 * 
	 * @param p
	 *            Program object which is the program that the computer shall
	 *            load and execute within its memory.
	 */
	public void load(Program p) {
		this.prog = p;
		this.counter = new Counter();
	}

	/**
	 * Run method runs the loaded program by executing all instructions given by
	 * the program.
	 */
	public void run() {
		while (true) {
			prog.get(counter.getPos()).exec(mem, counter);
		}
	}
}
