package Computer;

import Instruction.Add;
import Instruction.Copy;
import Instruction.Halt;
import Instruction.Jump;
import Instruction.JumpEq;
import Instruction.Mul;
import Instruction.Print;

/**
 * Factorial class extends the Program class and is a list of instructions that
 * calculates the factorial of 5.
 * 
 * @author Alexander Löfqvist
 *
 */
public class Factorial extends Program {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Factorial() {
		Address n = new Address(0), fac = new Address(1);
		add(new Copy(new LongWord(5), n));
		add(new Copy(new LongWord(1), fac));
		add(new JumpEq(6, n, new LongWord(1)));
		add(new Mul(fac, n, fac));
		add(new Add(n, new LongWord(-1), n));
		add(new Jump(2));
		add(new Print(fac));
		add(new Halt());
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		for (int i = 0; i < this.size(); i++) {
			st.append(i);
			st.append(" ");
			st.append(this.get(i).toString());
			st.append("\n");
		}
		return st.toString();
	}
}
