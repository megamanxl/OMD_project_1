package Computer;
/**
 * Counter class works as a program counter and have methods to manipulate the counter position.
 * System exit if the counter is set to -1.
 * @author Alexander Löfqvist
 *
 */
public class Counter {
	private int pos = 0;
	/**
	 * Construct
	 */
	public Counter() {

	}
	/**
	 * Method to get the current position of the counter.
	 * @return integer which is the current position.
	 */
	public int getPos(){
		return this.pos;
	}
	/**
	 * Method to increment the counter by one.
	 */
	public void inc() {
		this.pos++;
	}
	/**
	 * Method to set the counter to a given position.
	 * @param pos integer which is a position where the counter should be set.
	 */
	public void set(int pos) {
		this.pos = pos;
		if(this.pos<0){
			System.exit(1);
		}
	}
	/**
	 * Method to decrease the counter by one.
	 */
	public void dec() {
		this.pos--;
		if(this.pos<0){
			System.exit(1);
		}
	}
	@Override
	public String toString(){
		return String.valueOf(this.pos);
	}
}
