package Computer;
/**
 * Operand interface.
 * @author Alexander Löfqivst
 *
 */
public interface Operand {
	/**
	 * Method to return a word in a specified memory.
	 * @param m Memory object which is the memory from where the word should be fetched.
	 * @return
	 */
	public Word getWord(Memory m);
}
