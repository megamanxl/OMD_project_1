package Computer;

/**
 * LongWord class extends the Word class and holds it's value in form of the
 * primitive type of long.
 * 
 * @author Alexander Löfqivst
 *
 */
public class LongWord extends Word {
	private long value;
	/**
	 * Construct.
	 * @param value the value that should be assigned to this word.
	 */
	public LongWord(long value) {
		this.value = value;

	}
	
	@Override
	public Word getWord(Memory m) {
		return this;
	}
	/**
	 * Method to return the value held by this LongWord.
	 * @return long integer of the value that this word was assigned.
	 */
	public long getValue() {
		return this.value;
	}

	@Override
	public void mul(Word w) {
		LongWord lw = (LongWord) w;
		this.value = this.value * lw.getValue();

	}

	@Override
	public void add(Word w) {
		LongWord lw = (LongWord) w;
		this.value = this.value + lw.getValue();

	}

	@Override
	public boolean equals(Word w) {
		LongWord lw = (LongWord) w;
		return lw.getValue() == this.value;
	}

	@Override
	public void print() {
		System.out.print(this.value);

	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
}
