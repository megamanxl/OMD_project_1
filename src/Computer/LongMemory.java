package Computer;
/**
 * LongMemory class extends the Memory class and holds LongWords in its memory slots.
 * @author Alexander Löfqvist
 *
 */
public class LongMemory extends Memory {
	/**
	 * Construct
	 * @param length integer that tells which length the memory shall have.
	 */
	public LongMemory(int length) {
		super(length);
		super.memArray= new LongWord[super.length];
	}
	@Override
	public LongWord read(int index) {
		return (LongWord) super.read(index);
	}
}
