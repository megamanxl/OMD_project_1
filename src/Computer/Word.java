package Computer;
/**
 * Abstract class to describe necessary methods for a Word.
 * @author Alexander Löfqvist
 *
 */
public abstract class Word implements Operand {
	/**
	 * Method to multiply to words.
	 * @param w Word that the current word should be multiplied with.
	 */
	public abstract void mul(Word w);
	/**
	 * Method to add two words
	 * @param w Word that the current word should be added with.
	 */
	public abstract void add(Word w);
	/**
	 * Method to check if two words has to the same value.
	 * @param w Word that is to be checked if it is equal to the current word.
	 * @return true if equal else false.
	 */
	public abstract boolean equals(Word w);
	/**
	 * Method to print the value of the word.
	 */
	public abstract void print();
}
