package Computer;
/**
 * Address class works as a reference to a position in the memory.
 * @author Alexander Löfqvist
 *
 */
public class Address implements Operand {
	private int pos;
	/**
	 * Construct
	 * @param pos integer number which tells which position in the memory this address refers to.
	 */
	public Address(int pos) {
		this.pos = pos;
	}
	@Override
	public Word getWord(Memory m) {
		return m.read(this.pos);
	}
	/**
	 * Method to fetch the position this address refers to.
	 * @return integer with the position this address refers to.
	 */
	public int getPos(){
		return this.pos;
	}
	@Override
	public String toString(){
		return "[" + String.valueOf(this.pos) + "]";
	}
}
