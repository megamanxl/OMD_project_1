package Instruction;

import Computer.Address;
import Computer.Memory;
import Computer.Operand;
import Computer.Word;

/**
 * Add class is an instruction that adds two Operand objects together.
 * 
 * @author Alexander Löfqvist.
 *
 */
public class Add extends Arithmetic {
	/**
	 * Construct
	 * 
	 * @param op1
	 *            Operand that should be added with op2
	 * @param op2
	 *            Operand that should be added with op1
	 * @param a
	 *            Address where the result should be put in the memory.
	 */
	public Add(Operand op1, Operand op2, Address a) {
		super(op1, op2, a);
	}

	@Override
	protected void op(Word w1, Word w2, Address a, Memory m) {
		w1.add(w2);
		if (!m.write(a.getPos(), w1)) {
			System.out.println("Add intstuction failed!");
		}
	}

	@Override
	public String toString() {
		return "ADD " + super.operand1.toString() + " " + super.operand2.toString();
	}
}
