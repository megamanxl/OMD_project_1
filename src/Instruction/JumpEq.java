package Instruction;

import Computer.Counter;
import Computer.Memory;
import Computer.Operand;

/**
 * JumpEq class is an jump instruction that only jumps if the to given operands
 * are equal.
 * 
 * @author Alexander Löfqvist
 *
 */

public class JumpEq extends Jump {
	private Operand operand1;
	private Operand operand2;

	/**
	 * Construct.
	 * 
	 * @param pos
	 *            integer to which position to jump.
	 * @param op1
	 *            Operand that is to be checked for equality with op2.
	 * @param op2
	 *            Operand that is to be checked for equality with op1.
	 */
	public JumpEq(int pos, Operand op1, Operand op2) {
		super(pos);
		this.operand1 = op1;
		this.operand2 = op2;
	}

	@Override
	public void exec(Memory m, Counter c) {
		if (this.operand1.getWord(m).equals(this.operand2.getWord(m))) {
			c.set(super.pos);
		} else {
			c.inc();
		}
	}

	@Override
	public String toString() {
		return "JEQ " + super.pos + " " + this.operand1.toString() + " " + this.operand2.toString();
	}
}
