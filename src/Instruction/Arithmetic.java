package Instruction;

import Computer.Address;
import Computer.Counter;
import Computer.Memory;
import Computer.Operand;
import Computer.Word;

/**
 * Arithmetic is an abstract class that works as a template for arithmetic
 * instructions.
 * 
 * @author Alexander Löfqvist
 *
 */
public abstract class Arithmetic implements Instruction {
	protected Operand operand1;
	protected Operand operand2;
	protected Address address;

	/**
	 * method to perform the arithmetic operation.
	 * 
	 * @param w1
	 *            Word that the operation works on.
	 * @param w2
	 *            Word that the operation works on.
	 * @param a
	 *            Address where the result should be written.
	 * @param m
	 *            Memory which memory that the result should be written to.
	 */
	protected abstract void op(Word w1, Word w2, Address a, Memory m);

	/**
	 * Construct
	 * @param op1 Operand to make an arithmetic operation on.
	 * @param op2 Operand to make an arithmetic operation with.
	 * @param a Address.
	 */
	protected Arithmetic(Operand op1, Operand op2, Address a) {
		this.operand1 = op1;
		this.operand2 = op2;
		this.address = a;
	}
	@Override
	public void exec(Memory m, Counter c) {
		op(this.operand1.getWord(m), this.operand2.getWord(m), this.address, m);
		c.inc();
	}
}
