package Instruction;

import Computer.Counter;
import Computer.Memory;
import Computer.Operand;
/**
 * Print class is an instruction to print an given operand.
 * @author Alexander Löfqvist
 *
 */
public class Print implements Instruction {
	Operand operand;
	/**
	 * Construct
	 * @param o Operand that should be printed.
	 */
	public Print(Operand o) {
		this.operand = o;
	}

	@Override
	public void exec(Memory m, Counter c) {
		this.operand.getWord(m).print();
		c.inc();
	}
	@Override
	public String toString(){
		return "PRT";
	}
}
