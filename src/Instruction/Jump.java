package Instruction;

import Computer.Counter;
import Computer.Memory;

/**
 * Jump class is an instruction that jumps to a certain instruction, by
 * manipulate the counter.
 * 
 * @author Alexander Löfqvist
 *
 */
public class Jump implements Instruction {
	protected int pos;

	/**
	 * Construct.
	 * 
	 * @param pos
	 *            integer that tells which instruction to jump to.
	 */
	public Jump(int pos) {
		this.pos = pos;
	}

	@Override
	public void exec(Memory m, Counter c) {
		c.set(this.pos);
	}

	@Override
	public String toString() {
		return "JMP " + this.pos;
	}
}
