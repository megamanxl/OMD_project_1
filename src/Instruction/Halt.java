package Instruction;

import Computer.Counter;
import Computer.Memory;

/**
 * Class to end a process.
 * @author Alexander löfqvist
 *
 */
public class Halt implements Instruction {
	/**
	 * Construct
	 */
	public Halt() {

	}

	@Override
	public void exec(Memory m, Counter c) {
		c.set(-1);
	}

	@Override
	public String toString() {
		return "HLT";
	}
}
