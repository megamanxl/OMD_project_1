package Instruction;

import Computer.Address;
import Computer.Counter;
import Computer.Memory;
import Computer.Operand;
/**
 * Copy class is an instruction to copy a given operand to an address in the memory.
 * @author Alexander Löfqvist.
 *
 */
public class Copy implements Instruction {
	Operand operand;
	Address address;
	/**
	 * Construct 
	 * @param op1 Operand that should be copied. 
	 * @param addr Address where the operand should be copied to.
	 */
	public Copy(Operand op1, Address addr) {
		this.address = addr;
		this.operand = op1;
	}

	@Override
	public void exec(Memory m, Counter c) {
		if(!m.write(this.address.getPos(), operand.getWord(m))){
			System.out.println("Copy instruction failed!");
		}
		c.inc();
	}
	@Override
	public String toString(){
		return "CPY " + operand.toString() + " " + address.toString();
	}
}
