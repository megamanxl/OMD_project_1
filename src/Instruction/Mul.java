package Instruction;

import Computer.Address;
import Computer.Memory;
import Computer.Operand;
import Computer.Word;
/**
 * Mul class is an instruction that multiplies two Operand objects together.
 * 
 * @author Alexander Löfqvist.
 *
 */
public class Mul extends Arithmetic{
	/**
	 * Construct
	 * 
	 * @param op1
	 *            Operand that should be multiplied with op2
	 * @param op2
	 *            Operand that should be multiplied with op1
	 * @param a
	 *            Address where the result should be put in the memory.
	 */
	public Mul(Operand op1, Operand op2, Address a) {
		super(op1, op2, a);
	}
	@Override
	protected void op(Word w1, Word w2, Address a, Memory m) {
		w1.mul(w2);
		if(!m.write(a.getPos(), w1)){
			System.out.println("Mul instruction failed!");
		}
	}
	@Override
	public String toString(){
		return "MUL " + super.operand1.toString() + " " + super.operand2.toString();
	}
}
