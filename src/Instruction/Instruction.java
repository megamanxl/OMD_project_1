package Instruction;

import Computer.Counter;
import Computer.Memory;
/**
 * Instruction interface.
 * @author Alexander Löfqvist
 *
 */
public interface Instruction {
	/**
	 * Method to execute a instruction.
	 * @param m Memory, which memory to execute the instruction.
	 * @param c Counter, which counter this execution affects.
	 */
	public void exec(Memory m,Counter c);
}
